**README**

This repository is used to for version control of Cognos BI content:

* Report XML specifications
* Framework Manager project files
* Transformer model files (*.mdl)
* Deployment files

### How do I get set up? ###

* Summary of set up
* Configuration
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact